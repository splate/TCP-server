#!/usr/bin/env python

from __future__ import print_function
import socket
import sys
from datetime import datetime
from os import getloadavg
from os import fork
import psutil

class TCPServer():
    
    def __init__(self,host,port,debug):
        self.debug = debug
        self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_address = (host,port)
        self.lprint("starting the server")
        self.sock.bind(self.server_address)
        self.lprint("start to listen on %s and %s" % self.server_address)
        self.sock.listen(1)
        self.connection = None
        self.client_address = None


    def lprint(self, x):
        if self.debug == 1:
            print(x, file=sys.stderr)

    def exit():
        print("ebee")

    def date(self):
        self.lprint("date");
        return (datetime.now().strftime("%H:%M:%S %y/%m/%d").encode('utf-8'))

    def user(self):
        self.lprint ("user info")
        usr=[]
        for u in psutil.users():
            usr.append(u[0]) 
        return (" ".join(usr).encode('utf-8'))
    
                
    def top(self):
        self.lprint ("on demande l'sage des ressources")
        return (str(getloadavg()).encode('utf-8'))        
    
    def menu():
        print ("menu function")
    
    def get_request(self):
        choix = self.connection.recv(16)
        if choix:
            cmds = choix.decode()
            cmd = cmds.split(" ")[0].strip("\n")
        else:
            cmd = None
        return cmd
    

    commandes  = {
        "date" : date,
        "top" : top, 
        "user" : user,
        "menu" : menu,
        "exit" : exit,
    }


    def send_result(self,data):
        self.connection.sendall(data)
        
    def menu(self):
        menu="\nBienvenue\n\
les commandes possibles sont\n\
date-affiche la date\n\
top-affiche l'usage des ressouxse\n\
user - affiche les utilisateu\n\
menu - affiche le menu\n\
exit - sort du programme \n\n"
        return menu.encode('utf-8')
 
    def do_work(self):
        while True:
            self.connection, self.client_address = self.sock.accept()
            pid = fork()
            if pid == 0:
                self.lprint("nous sommes enfant")
                self.sock.close()
                try:
                    self.lprint("connection depuis %s sur le port %s" %(self.client_address[0],self.client_address[1]))
                    result = self.menu()
                    self.send_result(result)
                    while True:
                        self.lprint("recuperer la requete")
                        cmd = self.get_request()
                        if cmd is not None and cmd != 'exit':
                            self.lprint("verification de la commande")
                            if cmd in self.commandes.keys():
                                self.lprint("execunon de la commande")
                                res=self.commandes[cmd](self)
                            else:
                                res="commande inconnue".encode('utf-8')
                            self.send_result(res)
                        else:
                            self.lprint("fermoture de la connexion depuis %s",self.client_address[0])
                            exit()

                except Exception as e:
                    print ("oh-la-la, une erreur est survenue : %s"% str(e))

                finally:
                        self.lprint("nous fermous la connexion")
                        self.connection.close()
                        exit()
            else:
                self.lprint("we are into the parent")
                self.connection.close()

if __name__ == '__main__':
    serv = TCPServer('localhost',10002,1)
    serv.do_work()

#!/usr/bin/env python
import socket
import sys
sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address=('localhost', 10001)
print>>sys.stderr, 'Connexion vers %s sur le port %s'% server_address
sock.connect(server_address)
try:
  message='Bonjour tout le monde'
  print >>sys.stderr, 'Envoi du message %s' % message
  sock.sendall(message)
  octets_recus=0
  octets_attendus=len(message)
  while octets_recus<octets_attendus:
   data=sock.recv(16)
   octets_recus += len(data)
   print>>sys.stderr, 'nous aurons recu %s' %data
finally:
  print>>sys.stderr,'Fermeture de la socket'
  sock.close()
